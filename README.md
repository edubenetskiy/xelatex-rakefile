# The XeLaTeX Rakefile

This project aims to create a set of instructions
for building XeLaTeX documents with such a wonderful tool as Rake.

## Supported compilers

`xelatex` is the default compiler.
Feel free to rewrite the Rakefile to use other TeX compilers, e.g. `pdflatex`.

_Note_: The compilation is run _twice_ in order to generate PDF bookmarks correctly.
        It is due to some peculiarities of the `hyperref` package of LaTeX.

## Requirements

To use the Rakefile, you need to have a Ruby interpreter
with the `rake` gem installed.

## Fast Installation

Copy and paste this command into the terminal of your project's directory:

    curl https://gitlab.com/edubenetskiy/xelatex-rakefile/raw/master/Rakefile -fsSLO && chmod +x ./Rakefile

## Usage

Place the Rakefile into your project's directory and then
type one of the following commands in your terminal session.

- `rake` or `rake pdf` — builds all the TeX files in the current directory.
- `rake FILENAME.pdf` — builds a certain PDF file from `FILENAME.tex`.
- `rake clean` — removes intermediate files like `.aux`, `.toc`, `.log` etc.
- `rake clobber` — acts like `rake clean` but also removes generated PDF files.

Please notice that the `Rakefile` is executable itself
and you can invoke it another way:

    $ ./Rakefile FILENAME.pdf    # corresponds to `rake FILENAME.pdf`
    $ ./Rakefile clean           # corresponds to `rake clean`

etc.

## License

Copyright © 2015 Egor Dubenetskiy.

The program is available as open source under the terms of the MIT License.
See LICENSE.txt for further information.
