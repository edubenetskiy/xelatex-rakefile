# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).


## [Unreleased]

### Added
- Extensions `.snm` and `.nav` to the list of waste files.


## 0.0.1 — 2015-11-12

### Added
- Rakefile for compiling XeLaTeX source files to PDFs.
- Task for compiling all `.tex` files in the current working directory.
- Rule for providing tasks for each available `.tex` file.
- Task for cleaning intermediate and generated files.
- Execution rights for Rakefile and a Unix-style shebang for running it manually.


[Unreleased]: https://gitlab.com/edubenetskiy/xelatex-rakefile/compare/v0.0.1...HEAD
