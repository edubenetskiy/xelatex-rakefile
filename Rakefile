#!/usr/bin/env rake
#
# The XeLaTeX Rakefile, version 0.0.1.
#
# To build all the XeLaTeX files within the current directory,
# just type 'rake' in the terminal. See README.md for instructions.
#
# Copyright (c) 2015 Egor Dubenetskiy.
# The program is available as open source under the terms
# of the MIT License. See LICENSE.txt for further information.

require 'rake/clean'

WASTE_EXTENSIONS = %w(.aux .log .nav .out .snm .toc).freeze
TEX_FILES = FileList.new('*.tex')
PDF_FILES = TEX_FILES.ext('.pdf')

CLEAN.include WASTE_EXTENSIONS.flat_map { |extension| TEX_FILES.ext(extension) }
CLOBBER.include PDF_FILES

task default: :pdf

desc 'Compile all the XeLaTeX files into PDF'
task pdf: PDF_FILES

rule '.pdf' => '.tex' do |t|
  2.times { sh "xelatex #{t.source} #{t.name}" }
end
